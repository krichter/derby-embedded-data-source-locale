package de.richtercloud.derby.embedded.data.source.locale;

import java.sql.DriverManager;
import java.sql.SQLException;

public class Main {

    public static void main(String[] args) throws SQLException,
            ClassNotFoundException {
        System.out.println("running main");
        Class.forName("org.apache.derby.jdbc.ClientDriver");
        DriverManager.getConnection("jdbc:derby://localhost:1527/database", "user", "password");
            //should fail with error message
            //`java.sql.SQLNonTransientConnectionException: Die Verbindung wurde zurückgewiesen, weil die Datenbank database nicht gefunden wurde.`
            //because the database doesn't exist
    }
}
